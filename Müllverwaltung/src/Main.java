import java.io.BufferedReader; 
import java.io.File; 
import java.io.FileReader; 
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList; 

public class Main {

	public static String sql;
	public static Connection c = null;
	public static Statement stmt = null;
	public static String dbHost = "localhost"; // Hostname
	public static String dbPort = "3306";      // Port -- Standard: 3306
	public static String dbName = "test_db";   // Datenbankname
	public static String dbUser = "root";     // Datenbankuser
	public static String dbPass = "Starwars12";      // Datenbankpasswort

	public static ArrayList<String> ladeDatei(String datName) { 
		ArrayList<String> inserts=new ArrayList<String>();
		File file = new File(datName); 

		if (!file.canRead() || !file.isFile()) 
			System.exit(0); 

		BufferedReader in = null; 
		try { 
			in = new BufferedReader(new FileReader(datName)); 
			String zeile = null; 
			while ((zeile = in.readLine()) != null) { 
				//System.out.println("Gelesene Zeile: " + zeile); 
				inserts.add(zeile);
			} 
		} catch (IOException e) { 
			e.printStackTrace(); 
		} finally { 
			if (in != null) 
				try { 
					in.close(); 
				} catch (IOException e) { 
				} 
		}
		return inserts; 
	}

	public static void main(String[] args) throws SQLException, ClassNotFoundException {
		String dateiName = "/Users/florianbartenbach/Documents/4AHWII/INFI/INFI-DBS/java_dbs/inserts.rtf";
		ArrayList<String> values=new ArrayList<String>();
		
		Class.forName("com.mysql.jdbc.Driver"); // Datenbanktreiber für JDBC Schnittstellen laden.
		// Verbindung zur JDBC-Datenbank herstellen.
		c = DriverManager.getConnection("jdbc:mysql://"+dbHost+":"+ dbPort+"/"+dbName+"?"+"user="+dbUser+"&"+"password="+dbPass);
		
		values=ladeDatei(dateiName);

		create();
		
		for(int i=0;i<values.size();i++)
			insert("test", values.get(i));

	}

	public static void insert(String tabellenname, String values) throws SQLException{        
		stmt = c.createStatement();
		sql = "INSERT INTO " + tabellenname + " VALUES ("
				+ values
				+");";
		stmt.executeUpdate(sql);
		System.out.println("Inserted into Table " + tabellenname + " successfully");
		stmt.close();
	}
	
	public static void create() throws SQLException{
		stmt = c.createStatement();
        sql = "DROP TABLE IF EXISTS test;";
        stmt.executeUpdate(sql);
        stmt.close();

        stmt = c.createStatement();
        sql = "CREATE TABLE test(id INT, name TEXT, alter1 INT);";
        stmt.executeUpdate(sql);
        System.out.println("Table created successfully");
        stmt.close();
	}
}
