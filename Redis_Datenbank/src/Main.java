import java.util.List;

import redis.clients.jedis.Jedis;

public class Main {

	public static void main(String[] args) {
		//Connecting to Redis server on localhost 
		Jedis jedis = new Jedis("localhost"); 
		System.out.println("Connection to server sucessfully"); 
		//check whether server is running or not 
		System.out.println("Server is running: "+jedis.ping());
		
		//jedis.lrem("muelllaster",1,"*");
		jedis.del("muelllaster");
		jedis.del("Muelltonne");

		jedis.rpush("muelllaster", "IL-7865","typ-1","300l");
		jedis.rpush("Muelltonne", "MuellGmbH","30l");
		jedis.rpush("muelllaster", "IL-45H","typ-2","500l");
		jedis.rpush("muelllaster", "IL-MUE11","typ-1","200l");
		
		List<String> lasterliste = jedis.lrange("muelllaster", 0, -1);
		List<String> tonnenliste = jedis.lrange("Muelltonne", 0, -1);
		
		for(int i=0;i<lasterliste.size();i++){
			System.out.println(lasterliste.get(i)+", ");
			System.out.println();
		}
		
		for(int i=0;i<tonnenliste.size();i++){
			System.out.println(tonnenliste.get(i)+", ");
			System.out.println();
		}

	}


}
