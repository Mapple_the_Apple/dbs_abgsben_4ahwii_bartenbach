
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
@DatabaseTable(tableName = "dumptrucks")
public class Müllhersteller
{
    @DatabaseField(id = true)
    private String Ansprechpartner;
    @DatabaseField(canBeNull = false)
    private String name;
    @DatabaseField(canBeNull = false)
    private String Ort;
    @DatabaseField(canBeNull = false)
    private int PLZ;
    
    public String getAnsprechpartner() {
        return Ansprechpartner;
    }
    public void setAnsprechpartner(String Ansprechpartner) {
        this.Ansprechpartner = Ansprechpartner;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getOrt() {
        return Ort;
    }
    public void setOrt(String Ort) {
        this.Ort = Ort;
    }
    public int getPLZ() {
        return PLZ;
    }
    public void setPLZ(int PLZ) {
        this.PLZ = PLZ;
    }
    public Müllhersteller(){} //Default-Konstruktor (benötigt für ORMLite)
    public Müllhersteller(String Ansprechpartner, String name, String Ort, int PLZ) {
        super();
        this.Ansprechpartner = Ansprechpartner;
        this.name = name;
        this.Ort = Ort;
        this.PLZ = PLZ;
    }
    public static void main(String[] args)
    {
        
    }
}