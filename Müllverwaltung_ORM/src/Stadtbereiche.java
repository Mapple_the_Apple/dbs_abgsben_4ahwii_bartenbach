
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
@DatabaseTable(tableName = "dumptrucks")
public class Stadtbereiche
{
    @DatabaseField(id = true)
    private String bereichname;
    @DatabaseField(canBeNull = false)
    private int lkwNummer;
    
    
    public String getBereichname() {
        return bereichname;
    }
    public void setBereichnamen(String bereichname) {
        this.bereichname = bereichname;
    }
    public int getLkwNummer() {
        return lkwNummer;
    }
    public void setLkwNummer(int lkwNummer) {
        this.lkwNummer = lkwNummer;
    }
    
    public Stadtbereiche(){} //Default-Konstruktor (benötigt für ORMLite)
    public Stadtbereiche(String bereichname, int lkwNummer) {
        super();
        this.bereichname = bereichname;
        this.lkwNummer = lkwNummer;
    }
    public static void main(String[] args)
    {
        
    }
}