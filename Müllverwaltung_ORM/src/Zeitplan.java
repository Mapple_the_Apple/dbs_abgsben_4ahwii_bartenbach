
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
@DatabaseTable(tableName = "zeitplan")
public class Zeitplan
{
    @DatabaseField(id = true)
    private String datum;
    @DatabaseField(canBeNull = false)
    private String stadtbereich;
    @DatabaseField(canBeNull = false)
    private int lasternummer;
    
    public String getDatum() {
        return datum;
    }
    public void setDatum(String datum) {
        this.datum = datum;
    }
    public String getStadtbereichp() {
        return stadtbereich;
    }
    public void setStadtbereich(String stadtbereich) {
        this.stadtbereich = stadtbereich;
    }
    public int getLasternummer() {
        return lasternummer;
    }
    public void setLasternummer(int lasternummer) {
        this.lasternummer = lasternummer;
    }
    public Zeitplan(){} //Default-Konstruktor (benötigt für ORMLite)
    public Zeitplan(String datum, String stadtbereich, int lasternummer) {
        super();
        this.datum = datum;
        this.stadtbereich = stadtbereich;
        this.lasternummer = lasternummer;
    }
    public static void main(String[] args)
    {
        
    }
}