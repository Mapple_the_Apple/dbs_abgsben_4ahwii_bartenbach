
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
@DatabaseTable(tableName = "dumptrucks")
public class Mülltonne
{
    @DatabaseField(id = true)
    private String Modellname;
    @DatabaseField(canBeNull = false)
    private String hersteller;
    @DatabaseField(canBeNull = false)
    private int Fassungsvermögen;
    
    public String getModellname() {
        return Modellname;
    }
    public void setModellname(String Modellname) {
        this.Modellname = Modellname;
    }
    public String getHerstellerp() {
        return hersteller;
    }
    public void setHersteller(String hersteller) {
        this.hersteller = hersteller;
    }
    public int getFassungsvermögen() {
        return Fassungsvermögen;
    }
    public void setFassungsvermögen(int Fassungsvermögen) {
        this.Fassungsvermögen = Fassungsvermögen;
    }
    public Mülltonne(){} //Default-Konstruktor (benötigt für ORMLite)
    public Mülltonne(String Modellname, String hersteller, int Fassungsvermögen) {
        super();
        this.Modellname = Modellname;
        this.hersteller = hersteller;
        this.Fassungsvermögen = Fassungsvermögen;
    }
    public static void main(String[] args)
    {
        
    }
}