
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
@DatabaseTable(tableName = "dumptrucks")
public class MüllAuslehrZuordnung
{
    @DatabaseField(id = true)
    private String tonne;
    @DatabaseField(canBeNull = false)
    private int lkwNummer;
    
    
    public String getTonne() {
        return tonne;
    }
    public void setTonne(String tonne) {
        this.tonne = tonne;
    }
    public int getLkwNummer() {
        return lkwNummer;
    }
    public void setLkwNummer(int lkwNummer) {
        this.lkwNummer = lkwNummer;
    }
    
    public MüllAuslehrZuordnung(){} //Default-Konstruktor (benötigt für ORMLite)
    public MüllAuslehrZuordnung(String tonne, int lkwNummer) {
        super();
        this.tonne = tonne;
        this.lkwNummer = lkwNummer;
    }
    public static void main(String[] args)
    {
        
    }
}