
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.jdbc.JdbcConnectionSource;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;
public class ORMManager
{
    private final static String DATABASE_URL = "jdbc:sqlite:/Users/florianbartenbach/Documents/4AHWII/INFI/INFI-DBS/Datenbanken/müllverwaltung.db";
    
    private Dao<DumpTruck, Integer> dumptruckDao;
    private Dao<MüllAuslehrZuordnung, Integer> müllauslehrzuordnungDao;
    private Dao<Müllhersteller, Integer> müllherstellerDao;
    private Dao<Mülltonne, Integer> mülltonneDao;
    private Dao<Stadtbereiche, Integer> stadtbereicheDao;
    private Dao<Zeitplan, Integer> zeitplanDao;
    
    public static void main(String[] args) throws Exception {
        // turn our static method into an instance of Main
        new ORMManager().doMain(args);
    }
    
    private void doMain(String[] args) throws Exception {
        ConnectionSource connectionSource = null;
        try {
            // create our data-source for the database
            connectionSource = new JdbcConnectionSource(DATABASE_URL);
            // setup our database and DAOs
            setupDatabase(connectionSource);
            // read and write some data
            readWriteData();
            // do a bunch of bulk operations
            System.out.println("\n\nIt seems to have worked\n\n");
        } finally {
            // destroy the data source which should close underlying connections
            if (connectionSource != null) {
                connectionSource.close();
            }
        }
    }
    /**
     * Setup our database and DAOs
     */
    private void setupDatabase(ConnectionSource connectionSource) throws Exception {
        dumptruckDao = DaoManager.createDao(connectionSource, DumpTruck.class);
        müllauslehrzuordnungDao = DaoManager.createDao(connectionSource, MüllAuslehrZuordnung.class);
        müllherstellerDao = DaoManager.createDao(connectionSource, Müllhersteller.class);
        mülltonneDao = DaoManager.createDao(connectionSource, Mülltonne.class);
        stadtbereicheDao = DaoManager.createDao(connectionSource, Stadtbereiche.class);
        zeitplanDao = DaoManager.createDao(connectionSource, Zeitplan.class);
        // if you need to create the table
        TableUtils.createTable(connectionSource, DumpTruck.class);
        TableUtils.createTable(connectionSource, MüllAuslehrZuordnung.class);
        TableUtils.createTable(connectionSource, Müllhersteller.class);
        TableUtils.createTable(connectionSource, Mülltonne.class);
        TableUtils.createTable(connectionSource, Stadtbereiche.class);
        TableUtils.createTable(connectionSource, Zeitplan.class);
    }
    /**
     * Read and write some example data.
     */
    private void readWriteData() throws Exception {
        // create an instance of Account
        DumpTruck dumptruck1 = new DumpTruck("SZ-333L", "Volvo 1", 8000);
        MüllAuslehrZuordnung zuordnung1 = new MüllAuslehrZuordnung("34i",1);
        Müllhersteller hersteller1 = new Müllhersteller("Blum", "Ortner & Stanger", "Innsbruck", 6020);
        Mülltonne tonne1 = new Mülltonne("Supertonne", "Ortner & Stanger", 50);
        Stadtbereiche bereiche1 = new Stadtbereiche("Pradl", 12);
        Zeitplan zeitplan1 = new Zeitplan("12.12.18", "Pradl", 12);
        // persist the account object to the database
        dumptruckDao.create(dumptruck1);
        müllauslehrzuordnungDao.create(zuordnung1);
        müllherstellerDao.create(hersteller1);
        mülltonneDao.create(tonne1);
        stadtbereicheDao.create(bereiche1);
        zeitplanDao.create(zeitplan1);
    }
}